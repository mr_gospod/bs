﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ServerClient {
	class Program {
		static void Main(string[] args) {

			System.Console.Write("Server (1) /  Client (2): ");

			int iListen = Convert.ToInt32(System.Console.ReadLine());


			if (iListen == 1) {

				Server server = new Server();

				System.Console.Write("Server\nListening to port: 12345\n");

				server.localPort = 12345;

				server.Communicate();

			} else {

				Client client = new Client();

				System.Console.Write("Client\nConnecting to 127.0.0.1:12345\n");

				client.Communicate("127.0.0.1", 12345);

			}

			System.Console.Write("Communication stopped....");

			System.Console.ReadKey();

		}
	}
}
