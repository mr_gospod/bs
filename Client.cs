﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ServerClient {
	class Client {

		public TcpClient client;

		public void Communicate(string targetIP, int targetPort) {

			client = new TcpClient(targetIP, targetPort);

			string message = "Verbunden";


			Byte[] data = System.Text.Encoding.ASCII.GetBytes(message); // Translate the message into ASCII and store it as a Byte array. 


			NetworkStream stream = client.GetStream(); // Get a client stream for reading and writing.

			stream.Write(data, 0, data.Length); // Send the message to the connected TcpServer. 


			Console.WriteLine("Gesendet: {0}", message);

			
			data = new Byte[256]; // Receive the TcpServer.response. 

			String responseData = String.Empty; // String to store the response ASCII representation. 

			
			Int32 bytes = stream.Read(data, 0, data.Length); // Read the first batch of the TcpServer response bytes. 

			responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);


			Console.WriteLine("Bekommen: {0}", responseData);

			stream.Close(); // Close everything. 

			client.Close();

		}
	}
}
