﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ServerClient {
	class Server {
		public TcpListener server;

		public TcpClient client;

		public IPAddress localAddr;

		public int localPort;



		public void Communicate() {

			localAddr = IPAddress.Parse("127.0.0.1");

			server = new TcpListener(localAddr, localPort);



			server.Start();

			client = server.AcceptTcpClient(); // Perform a blocking call to accept requests. 			



			NetworkStream stream = client.GetStream(); // Get a stream object for reading and writing. 

			Byte[] bytes = new Byte[256];

			String data = null;



			int i;

			while ((i = stream.Read(bytes, 0, bytes.Length)) != 0) // Read incomming data. 

      {

				data = System.Text.Encoding.ASCII.GetString(bytes, 0, i); // Translate data bytes to a ASCII string. 

				Console.WriteLine("Bekommen: {0}", data);


				data = "Nachricht angekommen"; // Process the data sent by the client.

				byte[] msg = System.Text.Encoding.ASCII.GetBytes(data); // Send back a response. 

				stream.Write(msg, 0, msg.Length);

				Console.WriteLine("Gesendet: {0}", data);

			}
		}
	}
}
